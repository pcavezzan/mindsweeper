
${AnsiColor.BRIGHT_BLUE} __      __       .__                                  __                       
${AnsiColor.BRIGHT_BLUE}/  \    /  \ ____ |  |   ____  ____   _____   ____   _/  |_  ____               
${AnsiColor.BRIGHT_BLUE}\   \/\/   // __ \|  | _/ ___\/  _ \ /     \_/ __ \  \   __\/  _ \              
${AnsiColor.BRIGHT_BLUE}\        /\  ___/|  |_\  \__(  <_> )  Y Y  \  ___/   |  | (  <_> )             
${AnsiColor.BRIGHT_BLUE}  \__/\  /  \___  >____/\___  >____/|__|_|  /\___  >  |__|  \____/              
${AnsiColor.BRIGHT_BLUE}       \/       \/          \/            \/     \/                             
${AnsiColor.BRIGHT_BLUE}   _____  .__                _________                                          
${AnsiColor.BRIGHT_BLUE}  /     \ |__| ____   ____  /   _____/_  _  __ ____   ____ ______   ___________ 
${AnsiColor.BRIGHT_BLUE} /  \ /  \|  |/    \_/ __ \ \_____  \\ \/ \/ // __ \_/ __ \\____ \_/ __ \_  __ \
${AnsiColor.BRIGHT_BLUE}/    Y    \  |   |  \  ___/ /        \\     /\  ___/\  ___/|  |_> >  ___/|  | \/
${AnsiColor.BRIGHT_BLUE}\____|__  /__|___|  /\___  >_______  / \/\_/  \___  >\___  >   __/ \___  >__|   
${AnsiColor.BRIGHT_BLUE}        \/        \/     \/        \/             \/     \/|__|        \/       
${AnsiColor.BRIGHT_GREEN}:: Version ${application.version} ::${AnsiStyle.NORMAL}