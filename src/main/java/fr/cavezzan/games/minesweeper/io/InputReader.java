package fr.cavezzan.games.minesweeper.io;

/**
 * Réalise la lecture les entrées utilisateurs.
 */
public interface InputReader {
	
	/**
	 * Lit le paramètre saisie par l'utilisateur.
	 * 
	 * @param parameter Le paramètre dont la valeur est saisie par l'utilisateur.
	 * 
	 * @return La valeur.
	 */
	int read(String parameter);
	
}
