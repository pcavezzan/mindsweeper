package fr.cavezzan.games.minesweeper.io;

/**
 * Réalise l'écriture sur la sortie utilisateur.
 */
public interface OutputWriter {
	
	/**
	 * Ecrit l'objet.
	 * 
	 * @param object L'objet à afficher à l'utilisateur.
	 */
	void write(Object object);
	
	/**
	 * Ecrit un caractère.
	 * 
	 * @param c Le caractère à afficher à l'utilisateur.
	 */
	void write(char c);
	
	/**
	 * Ecrit une ligne vide.
	 */
	void writeLine();
	
	/**
	 * Ecrit sur une ligne un objet à afficher à l'utilisateur.
	 * 
	 * @param object L'objet.
	 */
	void writeLine(Object object);
	
	/**
	 * Ecrit sur une ligne un caractère à afficher à l'utilisateur.
	 * 
	 * @param c Le caractère.
	 */
	void writeLine(char c);
}
