package fr.cavezzan.games.minesweeper.io.impl;

import java.io.PrintStream;

import fr.cavezzan.games.minesweeper.io.OutputWriter;

/**
 * Implémentation de l'écriture sur une imprimante de donnée streamer.
 */
class PrintStreamOutputWriterImpl implements OutputWriter {
	
	/** L'imprimante par défaut utilisée est la sortie standard. */
	private PrintStream outputStream = System.out;
	
	/* (non-Javadoc)
	 * @see fr.cavezzan.games.minesweeper.io.OutputWriter#write(java.lang.Object)
	 */
	@Override
	public void write(final Object object) {
		outputStream.print(object);
	}
	
	/* (non-Javadoc)
	 * @see fr.cavezzan.games.minesweeper.io.OutputWriter#write(char)
	 */
	@Override
	public void write(final char c) {
		outputStream.write(c);
	}
	
	/* (non-Javadoc)
	 * @see fr.cavezzan.games.minesweeper.io.OutputWriter#writeLine()
	 */
	@Override
	public void writeLine() {
		outputStream.println();
	}
	
	/* (non-Javadoc)
	 * @see fr.cavezzan.games.minesweeper.io.OutputWriter#writeLine(java.lang.Object)
	 */
	@Override
	public void writeLine(final Object object) {
		outputStream.println(object);
	}
	
	/* (non-Javadoc)
	 * @see fr.cavezzan.games.minesweeper.io.OutputWriter#writeLine(char)
	 */
	@Override
	public void writeLine(char c) {
		outputStream.write(c);
	}
	
	public void setOutputStream(final PrintStream outputStream) {
		this.outputStream = outputStream;
	}
	
	public PrintStream getOutputStream() {
		return outputStream;
	}

}
