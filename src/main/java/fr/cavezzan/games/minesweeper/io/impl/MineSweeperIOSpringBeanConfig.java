package fr.cavezzan.games.minesweeper.io.impl;

import java.util.Scanner;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

/**
 * Configuration Spring des implémentations liées à nos entrées/sorties.
 */
@Configuration
public class MineSweeperIOSpringBeanConfig {

	@Bean
	@Scope(BeanDefinition.SCOPE_PROTOTYPE)
	public Scanner inputScanner() {
		return new Scanner(System.in);
	}

	
	@Bean
	public PrintStreamOutputWriterImpl outputWriterImpl() {
		return new PrintStreamOutputWriterImpl();
	}

	@Bean
	public ScannerInputStreamReaderImpl inputCliReader() {
		final ScannerInputStreamReaderImpl reader = new ScannerInputStreamReaderImpl();
		reader.setInput(inputScanner());
		return reader;
	}

}
