package fr.cavezzan.games.minesweeper;

import java.util.Random;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

/**
 * Application SpringBoot du démineur.
 * 
 * <p>
 * 	Cette application démarre un lanceur de jeu.
 * </p>
 * 
 * @see fr.cavezzan.games.minesweeper.game.runner.CliGameRunner
 */
@SpringBootApplication
public class MineSweeperCliApplication {
	
	@Bean
	public RandomizerImpl randomizerImpl() {
		final RandomizerImpl randomizer = new RandomizerImpl();
		randomizer.setEngine(new Random());
		return randomizer;
	}
	
	public static void main(String[] args) {
		SpringApplication.run(MineSweeperCliApplication.class, args);
	}
	
}
