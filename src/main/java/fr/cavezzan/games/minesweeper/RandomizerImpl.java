package fr.cavezzan.games.minesweeper;

import java.util.Random;

class RandomizerImpl implements Randomizer {
	
	private Random engine;
	
	@Override
	public int random(int bound) {
		return engine.nextInt(bound);
	}


	public Random getEngine() {
		return engine;
	}
	
	
	public void setEngine(final Random engine) {
		this.engine = engine;
	}
	
}
