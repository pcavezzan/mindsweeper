package fr.cavezzan.games.minesweeper.game;

/**
 * Gestionnaire d'une partie.
 */
public interface GameManager {
	
	/**
	 * Place la nouvelle position que l'utilisateur a choisi de jouer.
	 * 
	 * @param game La partie à jouer.
	 * 
	 * @return La nouvelle position choisie par l'utilisateur.
	 */
	Position nextPosition(Game game);
	
	/**
	 * Joue une partie.
	 * 
	 * @param game La partie à jouer.
	 * 
	 * @return L'état de la partie.
	 */
	GameStatus play(Game game);
	
	/**
	 * Caractérise l'état d'une partie:
	 * <ul>
	 * 	<li>doit continuer de jouer.</li>
	 * 	<li>a perdu.</li>
	 * 	<li>a gagné.</li>
	 * </ul>
	 */
	public enum GameStatus {
		PLAY,
		WIN,
		LOSE
	}
		
}
