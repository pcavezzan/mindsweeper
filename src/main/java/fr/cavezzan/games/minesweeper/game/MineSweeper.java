package fr.cavezzan.games.minesweeper.game;

import java.util.List;

/**
 * Définit la stratégie de déminage d'une partie.
 * 
 * <p>
 * 	La responsabilité de cette classe est de pouvoir gérer essentiellement les points 
 * 	de la grille d'un jeu en cours. 
 * </p>
 */
public interface MineSweeper {
	
	/**
	 * Pose les mines sur une partie de démineur.
	 * 
	 * @param game La partie.
	 * 
	 * @return Les positions de la partie qui ont été minées.
	 */
	List<Position> mines(final Game game);

	/**
	 * Effectue l'évaluation du nombre de mines entourant chaque point non miné d'une partie.
	 * 
	 * @param game La partie.
	 */
	void tips(final Game game);
	
	/**
	 * Révèle le contenu de la position choisie par le joueur.
	 * 
	 * @param game La partie.
	 * 
	 * @return vrai si la position est minée.
	 */
	boolean revealPosition(final Game game);
	
	/**
	 * Indique si la partie est gagné par l'utilisateur.
	 * 
	 * @param game La partie.
	 * 
	 * @return vrai si le joueur a gagné.
	 */
	boolean win(final Game game);
}
