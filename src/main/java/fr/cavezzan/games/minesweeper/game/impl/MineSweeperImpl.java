package fr.cavezzan.games.minesweeper.game.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.cavezzan.games.minesweeper.Randomizer;
import fr.cavezzan.games.minesweeper.game.Game;
import fr.cavezzan.games.minesweeper.game.Game.Point;
import fr.cavezzan.games.minesweeper.game.MineSweeper;
import fr.cavezzan.games.minesweeper.game.Position;

/**
 * Implémentation d'un démineur.
 * 
 * <p>
 * 	Cette classe regroupe la logique permettant de réaliser les actions du démineur, entre autres:
 * 	<ul>
 * 		<li>Poser des mines,</li>
 * 		<li>Indiquer le nombre de mines entourant un point,</li>
 * 		<li>Révéler un point choisie par un joueur,</li>
 * 		<li>Vérifier si un joueur a déminé l'ensemble des points,</li>
 * 	</ul>
 * </p>
 */
class MineSweeperImpl implements MineSweeper {
	
	/** Le logger. */
	private static final Logger LOG = LoggerFactory.getLogger(MineSweeperImpl.class);
	
	/** La valeur d'un point qui est considéré comme étant une mine. */
	static final int MINE = -1;

	/** Par défaut, une mine est un point dont la valeur est -1. */
	static final Predicate<Point> MINE_P = (p -> p.getValue() == MineSweeperImpl.MINE);


	/** La stratégie de génération de nombre aléatoire. */
	private Randomizer randomizer;
	
	/** 
	 * La fonction permettant de dénombrer l'ensemble des points adajacents à une position respectant une 
	 * condition. 
	 */
	private TipsNeighboorsPoints tipsNeighboorsPoints;
	
	/** 
	 * La fonction permettant de dénombrer l'ensemble des points adajacents à une position respectant une 
	 * condition. 
	 */
	private RevealNeighboorPoints revealNeighboorPoints;
	
	
	/* (non-Javadoc)
	 * @see fr.cavezzan.games.minesweeper.game.MineSweeper#mines(fr.cavezzan.games.minesweeper.game.Game)
	 */
	@Override
	public List<Position> mines(final Game game) {
		boolean raffled;
		final List<Position> minedPos = new ArrayList<>();
		for (int i = 0; i < game.getNbMines(); i++) {
			do {
				final int y = randomizer.random(game.getHeight()) + 1;
				final int x = randomizer.random(game.getWidth()) + 1;
				final Position pickedPos = Position.of(y, x);
				final int index = pickedPos.getX() + (pickedPos.getY() * (game.getWidth() + 2));
				final Point point = game.getData().get(index); 
				int pickedCase = point.getValue();
				if (pickedCase == -1) {
					LOG.debug("Position already mined ! Choosing another one.");
					raffled = true;					
				} else {
					raffled = false;
					point.setValue(MINE);
					minedPos.add(pickedPos);
					LOG.debug("Choosen position {} for mine.", pickedPos);
				}
			} while (raffled);
		}
		return minedPos;
	}
	
	/* (non-Javadoc)
	 * @see fr.cavezzan.games.minesweeper.game.MineSweeper#tips(fr.cavezzan.games.minesweeper.game.Game)
	 */
	@Override
	public void tips(final Game game) {
		game.getData().stream()
			.filter(po -> po.getValue() == 0)
			.filter((p -> p.getPosition().getX() != 0))
			.filter(p -> p.getPosition().getY() != 0)
			.filter(p -> p.getPosition().getX() <= game.getWidth())
			.filter(p -> p.getPosition().getY() <= game.getHeight())
			.forEach(p -> p.setValue(tipsNeighboorsPoints.apply(game, p.getPosition())));
	}
		
	/* (non-Javadoc)
	 * @see fr.cavezzan.games.minesweeper.game.MineSweeper#revealPosition(fr.cavezzan.games.minesweeper.game.Game)
	 */
	@Override
	public boolean revealPosition(final Game game) {
		final Position player = game.getPosition();
		LOG.debug("{} -->", player);
		final Point point = game.getPointAtPosition(player);
		boolean isAMine = MINE_P.test(point);
		point.setVisible(true);
		if (point.getValue() == 0) {
			LOG.debug("Display the closest point to {}", player);
			revealNeighboorPoints.apply(game, player);
		}

		if (isAMine && LOG.isDebugEnabled()) {
			LOG.debug("This point ({}) is a mine !", player);
		}
		
		return isAMine;
	}
	
	/* (non-Javadoc)
	 * @see fr.cavezzan.games.minesweeper.game.MineSweeper#win(fr.cavezzan.games.minesweeper.game.Game)
	 */
	@Override
	public boolean win(final Game game) {
		boolean hasWin = false;
		final int nbHiddenPoints = (int) game.getData().stream()
				.filter((p -> p.getPosition().getX() != 0))
				.filter(p -> p.getPosition().getY() != 0)
				.filter(p -> p.getPosition().getX() <= game.getWidth())
				.filter(p -> p.getPosition().getY() <= game.getHeight())
				.filter(Point::isHidden).count();
		if (nbHiddenPoints == game.getNbMines()) {
			LOG.debug("There is only mines on the grid, player has won !");
			hasWin = true;
		}
		
		return hasWin;
	}
	
	void setRandomizer(Randomizer randomizer) {
		this.randomizer = randomizer;
	}
	
	public Randomizer getRandomizer() {
		return randomizer;
	}

	void setRevealNeighboorPoints(
			RevealNeighboorPoints revealNeighboorPoints) {
		this.revealNeighboorPoints = revealNeighboorPoints;
	}
	
	public RevealNeighboorPoints getRevealNeighboorPoints() {
		return revealNeighboorPoints;
	}
	
	void setTipsNeighboorsPoints(TipsNeighboorsPoints tipsNeighboorsPoints) {
		this.tipsNeighboorsPoints = tipsNeighboorsPoints;
	}
	
	public TipsNeighboorsPoints getTipsNeighboorsPoints() {
		return tipsNeighboorsPoints;
	}
	
}
