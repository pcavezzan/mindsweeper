package fr.cavezzan.games.minesweeper.game.impl;

import fr.cavezzan.games.minesweeper.game.Game;
import fr.cavezzan.games.minesweeper.game.Game.Point;
import fr.cavezzan.games.minesweeper.game.Position;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.function.BiFunction;

/**
 * La fonction permettant de dénombrer l'ensemble des points adjacents à une position respectant une 
 * condition.
 */
class RevealNeighboorPoints implements BiFunction<Game, Position, Integer> {
	/** Le logger. */
	private static final Logger LOG = LoggerFactory.getLogger(RevealNeighboorPoints.class);

	@Override
	public Integer apply(final Game game, final Position position) {
		final List<Point> data = game.getData();
		Integer result = 0;
		
		for (int i = -1; i <= 1; i++) {
			for (int j = -1; j <= 1; j++) {
				final Position pos = Position.of(position.getY() + i, position.getX() + j);
				LOG.trace("Visiting {}", pos);
				if (game.isDisplayInGridPosition(pos)) {
					final Point point = game.getPointAtPosition(pos);
					if (point.getValue() != MineSweeperImpl.MINE && point.isHidden()) {
						LOG.debug("Revealing point {}", point);
						point.setVisible(true);
						result++;
						if (point.getValue() == 0) {
							LOG.debug("Point seems to not to be close to a mine, revealing it neightboorhood {}", point);
							result += apply(game, pos);
						}
					}
				}
			}
		}
		
		return result;
	}
}
