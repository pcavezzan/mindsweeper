package fr.cavezzan.games.minesweeper.game;

/**
 * Doit générer une partie d'un jeu à partir de paramètres.
 */
@FunctionalInterface
public interface GameGenerator {
	
	/**
	 * Effectue la génération de la partie.
	 * 
	 * @param parameters Les paramètres de la partie à générer.
	 * 
	 * @return La partie à jouer.
	 */
	Game generate();
	
}
