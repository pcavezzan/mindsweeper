package fr.cavezzan.games.minesweeper.game.impl;

import java.util.List;
import java.util.function.Predicate;

import fr.cavezzan.games.minesweeper.game.Game;
import fr.cavezzan.games.minesweeper.game.Game.Point;
import fr.cavezzan.games.minesweeper.game.GameRenderer;
import fr.cavezzan.games.minesweeper.game.Position;
import fr.cavezzan.games.minesweeper.io.OutputWriter;

class CliGameRenderer implements GameRenderer {
	
	private static final String POINT_SPACE = "   ";
	static final Predicate<Point> FIRST_ROW_P = (p -> p.getPosition().getX() == 0);
	static final Predicate<Point> LAST_ROW_P = (p -> p.getPosition().getX() == 0);
	static final Predicate<Point> FIRST_COLUMN_P = (p -> p.getPosition().getY() == 0);
	static final Predicate<Point> LAST_COLUMN_P = (p -> p.getPosition().getY() == 0);
	
	private Predicate<Point> renderPointPredicate;

	/** Le gestionnaire d'affiche de sortie. */
	private OutputWriter writer;
	
	@Override
	public Integer render(final Game game) {
		renderFirstLine();
		final List<Point> data = game.getData();
		
		data.stream()
			.filter((p -> p.getPosition().getX() != 0))
			.filter(p -> p.getPosition().getY() != 0)
			.filter(p -> p.getPosition().getX() <= game.getWidth())
			.filter(p -> p.getPosition().getY() <= game.getHeight())
			.forEach(point ->  {
			final Position pos = point.getPosition();
			final int index = pos.getX() + (pos.getY() * (game.getWidth() + 2));
			final Position previousPosition = data.get(index - 1).getPosition();
			renderNewLine(previousPosition);				
			writer.write(POINT_SPACE);
			renderPoint(point);
			
		});
		renderLastLine(game);
		return null;
	}

	private void renderFirstLine() {
		writer.writeLine();
		writer.writeLine("     Lines");
	}

	private void renderLastLine(final Game game) {
		// To show the last line number. ie: 0 1 2 3 Columns
		final StringBuilder columnGrid = new StringBuilder("         ");
		for (int i = 1; i <= game.getWidth(); i++) {
			columnGrid.append(POINT_SPACE).append(i);
		}
		writer.writeLine();
		writer.writeLine(columnGrid.toString());
		writer.writeLine("                      Columns");
	}

	private void renderPoint(final Point point) {
		if (point.isVisible()) {
			final int value = point.getValue();
			if (value == -1) {
				writer.write('*');
			} else {
				writer.write(value);
			}
		} else {
			writer.write('_');
		}
	}

	private void renderNewLine(final Position position) {
		if (position.getX() == 0) {
			writer.writeLine();
			writer.write("       " + position.getY() + " ");				
		}
	}

	public void setWriter(OutputWriter writer) {
		this.writer = writer;
	}
	
	public OutputWriter getWriter() {
		return writer;
	}
	
	public Predicate<Point> getRenderPointPredicate() {
		return renderPointPredicate;
	}
	
	public void setRenderPointPredicate(Predicate<Point> renderPointPredicate) {
		this.renderPointPredicate = renderPointPredicate;
	}
}
