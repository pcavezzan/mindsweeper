package fr.cavezzan.games.minesweeper.game.impl;

import fr.cavezzan.games.minesweeper.game.Game;
import fr.cavezzan.games.minesweeper.game.GameValidator;
import fr.cavezzan.games.minesweeper.game.Position;
import fr.cavezzan.games.minesweeper.io.OutputWriter;

/**
 * Implémentation d'un validateur de partie vis à vis de la position 
 * choisie du joueur.
 */
class PositionGameValidatorImpl implements GameValidator {

	private OutputWriter outputWriter;
	
	/* (non-Javadoc)
	 * @see fr.cavezzan.games.minesweeper.game.GameValidator#isValid(fr.cavezzan.games.minesweeper.game.Game)
	 */
	@Override
	public boolean isValid(final Game game) {
		boolean isValid = true;
		
		final Position player = game.getPosition();
		final Integer width = game.getWidth();
		final Integer height = game.getHeight();
		
		if (player.getY() < 0 || player.getY() > height 
				|| player.getX() < 0 || player.getX() > width) {
			outputWriter.writeLine("Choose a coordinate between ([1," + height + "];[1," + width + "]).");
			isValid = false;
		}
		return isValid;
	}
	
	public OutputWriter getOutputWriter() {
		return outputWriter;
	}
	
	public void setOutputWriter(OutputWriter outputWriter) {
		this.outputWriter = outputWriter;
	}

}
