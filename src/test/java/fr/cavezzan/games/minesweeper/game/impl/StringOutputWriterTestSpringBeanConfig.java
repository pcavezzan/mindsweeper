package fr.cavezzan.games.minesweeper.game.impl;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import fr.cavezzan.games.minesweeper.io.OutputWriter;

@Configuration 
class StringOutputWriterTestSpringBeanConfig {
	
	@Bean
	public StringBufferOutputWriter outputWriterImpl() {
		return new StringBufferOutputWriter();
	}
	
	static class StringBufferOutputWriter implements OutputWriter {
		
		private StringBuffer buffer = new StringBuffer();
		
		@Override
		public void write(Object object) {
			buffer.append(object);
		}

		@Override
		public void write(char c) {
			buffer.append(c);
		}

		@Override
		public void writeLine() {
			buffer.append(System.lineSeparator());
		}

		@Override
		public void writeLine(Object object) {
			buffer.append(object).append(System.lineSeparator());
		}

		@Override
		public void writeLine(char c) {
			buffer.append(c).append(System.lineSeparator());
		}
		
		public String flush() {
			final String result = buffer.toString();
			buffer.delete(0, buffer.length());
			return result;
		}
		
	}
}