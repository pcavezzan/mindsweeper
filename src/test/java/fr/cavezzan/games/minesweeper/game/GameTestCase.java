package fr.cavezzan.games.minesweeper.game;

import org.junit.Assert;
import org.junit.Test;

public class GameTestCase {

	@Test
	public void of() {
		final Game result = Game.of(8, 6, 6);
		Assert.assertNotNull("La fabrique aurait du créé le jeu.", result);
		Assert.assertEquals("Le partie est composé d'une matrice 10x8.", 80, result.getData().size());
		Assert.assertEquals("Le partie est composé de 6 mines.", 6, result.getNbMines());
		Assert.assertEquals("Le partie possède 6 lignes.", Integer.valueOf(6), result.getHeight());
		Assert.assertEquals("La partie possède 8 colonnes.", Integer.valueOf(8), result.getWidth());
		Assert.assertEquals("La partie est initialisée avec 0 coups.", 0, result.getCount());
	}
	@Test
	public void isDisplayInGridPosition() {
		final Game result = Game.of(8, 6, 6);
		Assert.assertTrue("La position (6,6) est sur bien visible sur le grid.", result.isDisplayInGridPosition(Position.of(6, 6)));
		Assert.assertTrue("La position (1,1) est sur bien visible sur le grid.", result.isDisplayInGridPosition(Position.of(1, 1)));
		Assert.assertTrue("La position (8,6) est sur bien visible sur le grid.", result.isDisplayInGridPosition(Position.of(8, 6)));
		Assert.assertFalse("La position (0,0) n'est pas visible sur le grid.", result.isDisplayInGridPosition(Position.of(0, 0)));
		Assert.assertFalse("La position (9,7) n'est pas visible sur le grid.", result.isDisplayInGridPosition(Position.of(9, 7)));
		Assert.assertFalse("La position (30,30) est en dehors du grid.", result.isDisplayInGridPosition(Position.of(30, 30)));
	}

}
