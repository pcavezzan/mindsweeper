package fr.cavezzan.games.minesweeper.game.runner;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import fr.cavezzan.games.minesweeper.game.Game;
import fr.cavezzan.games.minesweeper.game.GameGenerator;
import fr.cavezzan.games.minesweeper.game.GameManager;
import fr.cavezzan.games.minesweeper.game.GameManager.GameStatus;

@RunWith(MockitoJUnitRunner.class)
public class CliGameRunnerTestCase {
	
	@Mock
	private GameGenerator generator;
	
	@Mock
	private GameManager manager;
	
	@InjectMocks
	private CliGameRunner runner;

	@Before
	public void setUp() {
		final Game game = Game.of(8, 8, 10);
		Mockito.when(generator.generate()).thenReturn(game);
		Mockito.when(manager.play(game)).thenReturn(GameStatus.LOSE);
	}

	@Test
	public void runShouldGenerateAGame() throws Exception {
		runner.run();
		Mockito.verify(generator, Mockito.description("Le lanceur doit générer le jeu associé aux paramètres ."))
			.generate();
	}

	@Test
	public void runShouldKeepPlayingUntilGameIsOver() throws Exception {
		final Game game = Game.of(4, 4, 1);
		Mockito.when(generator.generate()).thenReturn(game);

		Mockito.when(manager.play(game)).thenReturn(GameStatus.PLAY, GameStatus.WIN);
		runner.run();
		Mockito.verify(manager, Mockito.times(2)).play(Mockito.any());
		Assert.assertEquals("La victoire s'est faite en 2 coups.", 2, game.getCount());
	}
	
}
