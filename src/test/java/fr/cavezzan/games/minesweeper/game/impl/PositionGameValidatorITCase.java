package fr.cavezzan.games.minesweeper.game.impl;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import fr.cavezzan.games.minesweeper.IntegrationTestConfiguration;
import fr.cavezzan.games.minesweeper.MineSweeperCliApplication;
import fr.cavezzan.games.minesweeper.game.Game;
import fr.cavezzan.games.minesweeper.game.Position;

@RunWith(SpringRunner.class)
@SpringBootTest(classes={MineSweeperCliApplication.class, IntegrationTestConfiguration.class})
public class PositionGameValidatorITCase {
	
	@Autowired
	private PositionGameValidatorImpl validator;

	@Test
	public void isValidShouldReturnTrueWhenPlayerHasChoosenCoordinatesOnGrid() {
		final Game game = Game.of(2, 2, 1);
		game.setPosition(Position.of(2, 2));
		final boolean result = validator.isValid(game);
		Assert.assertTrue("Quand le joueur choisi une case de grille du jeu, sa position est valide.", result);
	}
	
	@Test
	public void isValidShouldReturnFalseWhenPlayerHasChoosenCoordinatesOutsideTheGrid() {
		final Game game = Game.of(2, 2, 1);
		game.setPosition(Position.of(3, 3));
		final boolean result = validator.isValid(game);
		Assert.assertFalse("Quand le joueur choisi une case en dehors de la grille du jeu, sa position est valide.", 
			result);
	}
	
	@Test
	public void isValidShouldReturnFalseWhenPlayerHasChoosenOneCoordinateOutsideTheGrid() {
		final Game game = Game.of(2, 2, 1);
		game.setPosition(Position.of(1, 3));
		boolean result = validator.isValid(game);
		Assert.assertFalse(
			"Quand le joueur choisi une coordonnée en dehors de la grille du jeu, sa position est valide.", result);
		game.setPosition(Position.of(3, 1));
		result = validator.isValid(game);
		Assert.assertFalse(
			"Quand le joueur choisi une coordonnée en dehors de la grille du jeu, sa position est valide.", result);
	}

}
