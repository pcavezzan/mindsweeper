package fr.cavezzan.games.minesweeper.game.impl;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import fr.cavezzan.games.minesweeper.Randomizer;
import fr.cavezzan.games.minesweeper.game.Game;
import fr.cavezzan.games.minesweeper.game.Game.Point;
import fr.cavezzan.games.minesweeper.game.Position;


@RunWith(MockitoJUnitRunner.class)
public class MineSweeperImplTestCase {
	
	@Mock
	private Randomizer randomizer;
	
	@Mock
	private RevealNeighboorPoints revealNeighboorPoints;

	@Mock
	private TipsNeighboorsPoints tipsNeighboorsPointFunction;
	
	@InjectMocks
	private MineSweeperImpl mineSweeper;
	
	
	
	@Test
	public void minesShouldMineTheEntireInnerGrid() {
		final Game game = Game.of(2, 2, 4);
		
		Mockito.when(randomizer.random(2)).thenReturn(0, 0, 0, 1, 1 , 0, 1, 1);
		
		final List<Position> result = mineSweeper.mines(game);
		Assert.assertNotNull("Le mineur a normalement placé des mines.", result);
		Assert.assertFalse("Le mineur doit nous retourner une liste de position remplie.", result.isEmpty());
		Assert.assertEquals("La mineur doit avoir miné 4 positions.", 4, result.size());
		Assert.assertEquals("La première mine doit se trouver à (1, 1).", Position.of(1, 1), result.get(0));
		Assert.assertEquals("La première mine doit se trouver à (1, 2).", Position.of(1, 2), result.get(1));
		Assert.assertEquals("La première mine doit se trouver à (2, 1).", Position.of(2, 1), result.get(2));
		Assert.assertEquals("La première mine doit se trouver à (2, 2).", Position.of(2, 2), result.get(3));
	}
	
	
	@Test
	public void minesShouldMineTwoRandomPosition() {
		final Game game = Game.of(4, 6, 2);
		
		Mockito.when(randomizer.random(4)).thenReturn(3, 2);
		Mockito.when(randomizer.random(6)).thenReturn(1, 5);
		
		final List<Position> result = mineSweeper.mines(game);
		Assert.assertNotNull("Le mineur a normalement placé des mines.", result);
		Assert.assertFalse("Le mineur doit nous retourner une liste de position remplie.", result.isEmpty());
		Assert.assertEquals("La mineur doit avoir miné 2 positions.", 2, result.size());
		Assert.assertEquals("La première mine doit se trouver à (2, 4).", Position.of(2, 4), result.get(0));
		Assert.assertEquals("La première mine doit se trouver à (6, 3).", Position.of(6, 3), result.get(1));
	}
	
	@Test
	public void minesShouldNotMineAPositionAlreadyMined() {
		final Game game = Game.of(4, 6, 2);
		
		Mockito.when(randomizer.random(4)).thenReturn(3, 3, 2);
		Mockito.when(randomizer.random(6)).thenReturn(1, 1, 5);
		
		final List<Position> result = mineSweeper.mines(game);
		Assert.assertNotNull("Le mineur a normalement placé des mines.", result);
		Assert.assertFalse("Le mineur doit nous retourner une liste de position remplie.", result.isEmpty());
		Assert.assertEquals("La mineur doit avoir miné 2 positions.", 2, result.size());
		Assert.assertEquals("La première mine doit se trouver à (2, 4).", Position.of(2, 4), result.get(0));
		Assert.assertEquals("La première mine doit se trouver à (6, 3).", Position.of(6, 3), result.get(1));
	}
	
	@Test
	public void tipsShouldFillOutNumberOfMinesSurroundingPoint() {
		final Game game = Game.of(3, 3, 2);
		game.getData().get(13).setValue(-1);
		game.getData().get(6).setValue(-1);
		
		mineSweeper.tips(game);
		
		Mockito.verify(tipsNeighboorsPointFunction, Mockito.times(7)).apply(Mockito.eq(game), Mockito.any());
	}

	@Test
	public void revealPositionShouldReturnTrueAndDisplayItIfItIsAMine() {
		final Game game = Game.of(3, 3, 1);
		game.setPosition(Position.of(2, 3));
		game.getData().get(13).setValue(-1);
		
		final boolean result = mineSweeper.revealPosition(game);
		
		Assert.assertTrue("La révélation d'une position minée doit renvoyer vrai.", result);
		Assert.assertTrue("La position minée doit devenir visible", game.getData().get(13).isVisible());
	}
	
	@Test
	public void revealPositionShouldReturnFalseIfItIsNotAMine() {
		final Game game = Game.of(3, 3, 1);
		game.setPosition(Position.of(2, 3));
		game.getData().get(12).setValue(-1);
		
		final boolean result = mineSweeper.revealPosition(game);
		
		Assert.assertFalse("La révélation d'une position non minée doit renvoyer false.", result);
	}

	@Test
	public void revealPositionShouldDisplayPointAndItNeighboorIfItIsNotAMine() {
		final Game game = Game.of(3, 3, 1);
		game.setPosition(Position.of(2, 3));
		game.getData().get(12).setValue(-1);
		
		mineSweeper.revealPosition(game);

		Assert.assertTrue("La position du joueur doit être révélée.", game.getPointAtPosition(game.getPosition()).isVisible());
		
		Mockito.verify(revealNeighboorPoints,
			Mockito.description("L'inspection de la position choisie et ses voisins au sein de la grille de jeu doit être lancée."))
			.apply(Mockito.eq(game), Mockito.eq(game.getPosition()));
	}

	@Test
	public void revealPositionShouldDisplayOnlyPointIfItIsSurroundedByAMine() {
		final Game game = Game.of(3, 3, 1);
		game.setPosition(Position.of(2, 3));
		game.getPointAtPosition(game.getPosition()).setValue(1);
		game.getData().get(12).setValue(-1);

		mineSweeper.revealPosition(game);

		Assert.assertTrue("La position du joueur doit être révélée.", game.getPointAtPosition(game.getPosition()).isVisible());

		Mockito.verify(revealNeighboorPoints,
				Mockito.times(0).description("L'inspection de la position choisie et ses voisins au sein de la grille de jeu doit être lancée."))
				.apply(Mockito.eq(game), Mockito.eq(game.getPosition()));
	}

	@Test
	public void winShouldReturnFalse() {
		final Game game = Game.of(2, 2, 1);
		final boolean result = mineSweeper.win(game);
		Assert.assertFalse("Le jeu n'est pas gagnée.", result);
	}
	
	@Test
	public void winShouldReturnTrue() {
		final Game game = Game.of(2, 2, 1);
		final List<Point> grid = game.getData();
		grid.get(5).setVisible(true);
		grid.get(6).setVisible(true);
		grid.get(10).setVisible(true);
		
		final boolean result = mineSweeper.win(game);
		
		Assert.assertTrue("Le jeu est pas gagnée.", result);
	}
	
}
