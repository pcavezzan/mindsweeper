package fr.cavezzan.games.minesweeper.io.impl;

import java.util.Scanner;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import fr.cavezzan.games.minesweeper.io.impl.ScannerInputStreamReaderImpl;

@RunWith(MockitoJUnitRunner.class)
public class ScannerInputStreamReaderImplTestCase {
	
	private ScannerInputStreamReaderImpl inputReader = new ScannerInputStreamReaderImpl();
	
	@Test
	public void readParameterShouldReturnValue() {
		inputReader.setInput(new Scanner("4"));
		final Integer result = inputReader.read("Width");
		Assert.assertNotNull("L'entrée devrait avoir été récupérée.", result);
		Assert.assertEquals("La valeur saisie vaut 4.", Integer.valueOf(4), result);
	}

	@Test
	public void readParameterShouldReturnTheSecondLineValue() {
		inputReader.setInput(new Scanner("ae" + System.lineSeparator() + "4"));
		final Integer result = inputReader.read("Width");
		Assert.assertNotNull("L'entrée devrait avoir été récupérée.", result);
		Assert.assertEquals("La valeur saisie vaut 4.", Integer.valueOf(4), result);
	}

}
