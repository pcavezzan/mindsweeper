package fr.cavezzan.games.minesweeper.io.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;

import org.junit.Assert;
import org.junit.Test;


public class PrintStreamOutputStreamReaderImplTest {
	
	private final PrintStreamOutputWriterImpl writer = new PrintStreamOutputWriterImpl();
	
	@Test
	public void writeObject() throws IOException {
		final ByteArrayOutputStream baos = new ByteArrayOutputStream();
		writer.setOutputStream(new PrintStream(baos));
		
		final String objectToWrite = "Welcome to MineSweeper";
		writer.write(objectToWrite);
		
		Assert.assertEquals("On devrait avoir écrit 'Welcome to MineSweeper'", objectToWrite, new String(baos.toByteArray()));
		baos.close();
	}

	@Test
	public void writeChar() throws IOException {
		final ByteArrayOutputStream baos = new ByteArrayOutputStream();
		writer.setOutputStream(new PrintStream(baos));
		
		final char charToWrite = '*';
		writer.write(charToWrite);
		
		Assert.assertEquals("On devrait avoir écrit '*'", charToWrite, new String(baos.toByteArray()).charAt(0));
		baos.close();
	}

	@Test
	public void writeLine() throws IOException {
		final ByteArrayOutputStream baos = new ByteArrayOutputStream();
		writer.setOutputStream(new PrintStream(baos));
		writer.writeLine();
		
		Assert.assertEquals("On devrait avoir écrit une ligne vide.", 
				System.lineSeparator(), new String(baos.toByteArray()));
		baos.close();
	}
	
	@Test
	public void writeLineObject() throws IOException {
		final ByteArrayOutputStream baos = new ByteArrayOutputStream();
		writer.setOutputStream(new PrintStream(baos));
		
		final String objectToWrite = "Welcome to MineSweeper";
		writer.writeLine(objectToWrite);
		
		Assert.assertEquals("On devrait avoir écrit 'Welcome to MineSweeper' suivi d' un retour à la ligne.", 
				objectToWrite + System.lineSeparator(), new String(baos.toByteArray()));
		baos.close();
	}
	
}
