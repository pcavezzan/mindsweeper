package fr.cavezzan.games.minesweeper.io.impl;

import java.util.Scanner;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.test.context.junit4.SpringRunner;

import fr.cavezzan.games.minesweeper.IntegrationTestConfiguration;
import fr.cavezzan.games.minesweeper.MineSweeperCliApplication;
import fr.cavezzan.games.minesweeper.io.impl.ScannerInputStreamReaderImplITCase.ScannerInputStreamReaderImplITCaseSpringBeanConfig;

@RunWith(SpringRunner.class)
@SpringBootTest(classes={MineSweeperCliApplication.class, 
		IntegrationTestConfiguration.class, ScannerInputStreamReaderImplITCaseSpringBeanConfig.class})
public class ScannerInputStreamReaderImplITCase {
	
	@Autowired
	private ScannerInputStreamReaderImpl inputReader;
	
	@Test
	public void readValueShouldReturnValueAssociatedWithParameters() {
		final Integer width = inputReader.read("Width:");
		Assert.assertNotNull("InputReader should have created a non null instance.", width);
		Assert.assertEquals("InputReader should have returned: width=7.", Integer.valueOf(7), width);
		final Integer height = inputReader.read("Height:");
		Assert.assertNotNull("InputReader should have created a non null instance.", height);
		Assert.assertEquals("InputReader should have returned: height=5.", Integer.valueOf(5), height);
	}
	
	@Configuration
	static class ScannerInputStreamReaderImplITCaseSpringBeanConfig {
		
		@Bean
		@Scope(BeanDefinition.SCOPE_PROTOTYPE)
		public Scanner inputScanner() {
			return new Scanner(
					"7" + System.lineSeparator() + 
					"5" + System.lineSeparator() +
					"4" + System.lineSeparator() +
					"7" + System.lineSeparator() + 
					"5" + System.lineSeparator() +
					"4" + System.lineSeparator() +
					"7" + System.lineSeparator() + 
					"5" + System.lineSeparator() +
					"4");
		}

	}
	
}
