# Mindsweeper

Ce projet est une implémentation du jeu du démineur qui a revisité [une solution sur laquelle je me suis inspiré](http://www.progressivejava.net/2012/10/How-to-make-a-Minesweeper-game-in-Java.html)

## Construction
$[mindsweeperDir]>mvn clean install

## Lancement du jeu
$[mindsweeperDir]>java -jar target/target/minesweeper-0.4.0-SNAPSHOT.jar

## Le principe du design de cette implémentation

Au lancement du programme, le jeu va récupérer les paramètres d'une partie (Implémentaion réalisée pour les récupérer depuis une invite de commande à l'utilisateur).
Ensuite le programme crée une partie associée aux paramètres trouvés puis un gestionnaire de partie prend le relais pour gérer les coups joués.

## Règles du jeu.
Tant que le joueur ne tombe pas sur une mine, le jeu se poursuit.
Il consiste à dévoiler la position choisie par le joueur ainsi que ses premiers voisins non minés. 

La partie se finit si le joueur tombe sur une mine (il a alors perdu) ou s'il ne reste plus que des mines sur la grille de jeu (le joueur a alors gagné). 